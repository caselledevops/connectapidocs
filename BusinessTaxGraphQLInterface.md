﻿The Business Tax Collection GraphQL Interface
=============================================

This document discusses the GraphQL interface for Business Tax Collection in detail and assumes you are
already familiar with GraphQL and Caselle' implementation. See [Caselle GraphQL](CaselleGraphQL.html)
for an introduction to Caselle's implementation of GraphQL.

Business Tax Collection GraphQL Schema Overview
-----------------------------------------------

The Business Tax Collection GraphQL endpoint at `/tp/api/blt/graphql/` was developed to accept GraphQL requests
that conform to a Caselle developed GraphQL schema. The schema defines the types and the type fields that can be queried. For
example consider the following GraphQL query:

```GraphQL
query Schema {
  business(id: 1) {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}
```

The above query invokes the schema type `business` and is requesting the specific business where the ID is 1. If
a business is found just the id, account number, manager's full name, and the tax returns for the business are
returned. All the types that can be queried and the fields on those types are documented below.

#### <a name="BusinessType"></a>Business type

**Fields**

|Field name            |Type             |Notes|
|----------------------|-----------------|-----|
|id                    |integer type     |
|accountNumber         |integer type     |
|locationCity          |string type      |
|locationState         |string type      |
|mailingContactInfo    |string type      |
|businessTelephone1    |string type      |
|businessTelephone2    |string type      |
|managerName           |[Name type](#NameType)
|managerContactInfo    |[Contact Info type](#ContactInfoType)
|managerMailRoute      |string type     |
|businessName          |string type     |
|businessEmail         |string type     |
|businessFax           |string type     |
|businessStatus        |string type     |Valid values: "Active", "Closed", "Denied", "Exempt", "Inactive", </br>"Non-Filer", "Pending", "Suspended"
|salesTaxId            |string type     |
|alertMessage          |string type     |
|businessClassifaction |string type     |
|DBA                   |string type     |
|originationDate       |[Date Time type](#DateTimeType) |
|federalId             |string type     |
|stateId               |string type     |
|locationCountry       |string type     |
|locationDeliveryPoint |string type     |
|locationMailRoute     |string type     |
|location              |[Parsed Street Address type](#ParsedStreetAddressType) |
|mailingRoute          |string type     |
|mailingAttention      |[Name type](#NameType) |
|businessClosedDate    |[Date Time type](#DateTimeType) |
|businessActivity      |[Business Activity type](#BusinessActivityType) |
|contactGuid           |string type     |
|PIN                   |integer type    |
|guid                  |string type     |
|sendPaperReturn       |boolean type    |
|sendEmail             |boolean type    |
|taxStatus             |string type     |Valid values: "Active", "Closed", "Denied", "Exempt", "Inactive", </br>"Non-Filer", "Pending", "Suspended"
|taxReturns            |[Tax Return type](#TaxReturnType) |An array of all tax returns. </br>Optionally the returns could be filtered depending on how they are queried.

**How to Query Businesses**

All the above fields can be queried using the "business", "business_filtered_returns", "business_tax_returns_by_year",
and "business_tax_returns_by_date_range" built in queries. But only certain fields can be used as query parameters.
The "business" query does not filter the `taxReturns` array if it is referenced. But the other queries are specifically
setup to filter the tax returns based on different criteria.

*Using the "business" query:* The "business" query can take one of three different parameters. You can query by ID, by sales tax ID,
or by account number. The following queries return the exact same data as long as the ID, sales tax ID, and account number
identify the same business:

```graphql
query Schema {
  business(id: 1) {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}

query Schema {
  business(accountNumber: 1010) {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}

query Schema {
  business(salesTaxId: "5A0001") {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}

```

*Using the "business_filtered_returns" query:* This query takes two parameters, the account number of the business you
want to query and the tax return status. This query is exactly the same as the "business" query but it filters
the `taxReturns` array by tax return status. The valid values for the tax return status are "Completed", "Disputed",
"Filed", "Non-filed", "Pending", and "Void". If you use a tax return status and the business has no tax returns with
the requested status, the `taxReturns` array will be empty. Here is an example query:

```
query Schema {
  business_filtered_returns(accountNumber: 1010, status: "Non-filed") {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}
```

*Using the "business_tax_returns_by_year" query*. This query takes two parameters, the account number and a integer value
for the year you wish to filter by. The filter is applied to the tax returns "returnFiledDate". Any returns filed
for that year will be returned. Here's an example query:

```
query Schema {
  business_tax_returns_by_year(accountNumber: 1010, year: 2010) {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}
```

*Using the "business_tax_returns_by_date_range" query.* This query takes four parameters. The parameters are
"accountNumber" (required), "startDate" and "endDate" (both required), and "status". The
"status" parameter is not required but the other three are. The "startDate" and "endDate" filter on the
"taxPeriodStartDate" and the "taxPeriodEndDate" respectively. Here is an example query:

```graphql
query Schema {
  business_tax_returns_by_date_range(accountNumber: 1010, startDate: "", status: "Non-filed") {
    id,
    accountNumber,
    managerName {
      fullName
    },
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}
```

You can also use the `businesses` query to search for one or more businesses that match the query criteria. You can
also use this query to return all businesses but performance will be impacted when querying all businesses. You can
pass the following criteria to the "businesses" query, "businessName", "DBA", "mailingAddress", "locationAddress",
and "businessStatus". The "businessStatus" parameter can be used with the other parameters but the other four parameters
are exclusive. You can't search for a business with names that match the "businessName" parameter's value and
also match the "DBA" parameter's value. Here are some example queries with explanations of how the data is filtered.

```graphql
query Schema {
  businesses(businessName: "Pizza", businessStatus: "Active") {
    id,
    accountNumber,
    managerName {
      fullName
    },
  }
}
```
The query above will return an array of all businesses where the business name contains "Pizza" and the status is "Active".
The "businessStatus" parameter is optional. If no matches are found the array will be empty. The JSON returned by
the query would look like this:

```json
{
  "user_warnings": [],
  "user_errors": [],
  "status": 200,
  "server_error": "",
  "body": {
    "businesses": [{
      "id": 1,
      "businessName": "Pizza Plant",
      "accountNumber": 1010,
      "managerName": {
        "fullName": "John Doe"
      }
    },
    {
      "id": 2,
      "businessName": "Pizza Palace",
      "accountNumber": 1020,
      "managerName": {
        "fullName": "George of Jungle"
      }
    }]
  }
}
```

```qraphql
query Schema {
  businesses(DBA: "Anycity") {
    id,
    accountNumber,
    managerName {
      fullName
    }
  }
}
```

This query will return an array of all business where the DBA field contains the string "Anycity". The
"businessStatus" parameter is omitted so businesses are not filtered based on status.

**Note** When using the `businesses` query you can only query business data. You can't query any data related to
the business like tax returns or the business's activity data.

#### <a name="TaxReturnType"></a>Tax Return type

**Fields**

|Field name         |Type                 |Notes|
|-------------------|---------------------|-----|
|id                 |integer type         |
|returnDescription  |string type          |
|returnStatus       |string type          |Valid values: "Completed", "Disputed", "Filed", "Non-filed", <br/>"Pending", and "Void"
|taxPeriodEndDate   |[Date Time type](#DateTimeType)|
|taxPeriodStartDate |[Date Time type](#DateTimeType)|
|comment            |string type         |
|finalReturn        |boolean type        |
|sourceId           |string type         |
|taxReturnDueDate   |[Date Time type](#DateTimeType)|
|returnAmendedDate  |[Date Time type](#DateTimeType)|
|submitterName      |string type         |
|submitterTelephone |string type         |
|returnFiledDate    |[Date Time type](#DateTimeType)|
|taxReturnType      |[Tax Return Type type](#TaxReturnTypeType) |
|taxReturnDetails   |[Tax Return Detail type](#TaxReturnDetailType) |An array of all the tax return details for the return.
|balance            | decimal type      | The computed balance of the tax return. Will be null for tax returns that <br/> are not 'Complete', 'Filed', or 'Pending' status. Including the `balance` in your <br/> query will cause addition processing and IO to calculate the balance.  

**How to query tax returns**

You can use the `tax_return` query to retrieve a tax return by the tax return's ID. This GraphQL schema is designed to query
businesses and the tax returns related to those businesses so only this simple way to query a single tax return is provided. All the
fields listed above can be queried along with the tax return type and an array of the tax return details. The tax agency
can be queried on each tax return detail. If your query requests the `balance` field then additional database IO is performed
in order to calculate the correct amount. Keep this in mind when querying tax returns. If you dont' need the `balance` field
then don't query it. Also the `balance` field is only valid for tax returns with a `returnStatus` of 'Pending', 'Filed', and
'Completed'. `balance` will be null for other `returnStatus` values.  A `tax_return` query example:

```graphql
query Schema{
  tax_return(id: 3344) {
    id,
    returnDescription,
    returnStatus,
    returnFiledDate,
    taxReturnType {
      title
    },
    balance,
    taxReturnDetails {
      description,
      sequenceNumber,
      inputAmount,
      calculatedAmount
    }
  }
}
```

#### <a name="TaxReturnDetailType"></a>Tax Return Detail type

**Fields**

|Field name            |Type             |Notes|
|----------------------|-----------------|-----|
|id                    |integer type     |
|createTransaction     |boolean type     |
|description           |string type      |
|feeType               |boolean type     |
|sequenceNumber        |byte type        |The consumer of this GraphQL interface can treat the sequence <br/>number as an integer since it is never updated via the GraphQL interface.
|formula               |string type      |
|reference             |string type      |
|valueType             |string type      |Valid values: "Calculated", "Calculated Override", "Entered", <br/>"Entered Required", and "Fixed"
|isOverridden          |boolean type     |
|displayValue          |string type      |The formatted numeric value of the detail line.
|allowComments         |boolean type     |
|comment               |string type      |
|inputAmount           |double type      |
|amended               |boolean type     |
|adjustedAmount        |double type      |
|calculatedAmount      |double type      |
|reportCategory        |string type      |Valid values: "Deductions", "Fees due", "Gross amount", "None", <br/>"Taxable amount", "Taxes due", and "Total due"
|adjusted              |boolean type     |
|requiresDocumentation |boolean type     |
|taxAgency             |[Tax Agency type](#TaxAgencyType) |

#### <a name="TaxReturnTypeType"></a>Tax Return Type type

**Fields**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|id                 |integer type     |
|title              |string type      |
|dueDailyDays       |short type       |The Tax Return Type type uses the short and byte integer types <br/>because legacy reasons. JSON of course doesn't care.
|dueMonthlyMonths   |byte type        |
|dueMonthlyDayDay   |byte type        |
|dueMonthlyDayMonth |string type      |Valid values: "First", "Second", "Third", and "Fourth"
|dueMonthlyTheWeek  |string type      |Valid values: "First", "Second", "Third", "Fourth", and "Last"
|dueMonthlyTheDOW   |string type      |Valid values: "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", <br/>"Friday", "Saturday", and "Day"
|dueMonthlyTheMonth |string type      |Valid values: "First", "Second", "Third", and "Fourth"
|dueDaily           |boolean type     |
|dueMonthly         |boolean type     |
|dueMonthlyDay      |boolean type     |
|dueMonthlyThe      |boolean type     |

#### <a name="BusinessActivityType"></a>Business Activity type

**Fields**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|activity           |string type      |
|description        |string type      |



#### <a name="TaxAgencyType"></a>Tax Agency type

**Fields**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|id                 |integer type     |
|agency             |string type      |
|glArAccount        |string type      |
|glCashAccount      |string type      |
|glRevenueAccount   |string type      |
|glWriteOffAccount  |string type      |

#### <a name="NameType"></a>Name type

**Fields**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|fullName           |string type      |
|firstName          |string type      |
|middleName         |string type      |
|lastName           |string type      |
|suffix             |string type      |

#### <a name="ParsedStreetAddressType"></a>Parsed Street Address type

**Fields**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|houseNumber        |integer type     |
|houseNumberSuffix  |string type      |
|streetDirection    |string type      |
|street             |string type      |
|apartment          |string type      |
|streetAddress      |string type       |

#### <a name="ContactInfoType"></a>ContactInfo type

 **Fields**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|address1           |string type      |
|address2           |string type      |
|city               |string type      |
|state              |string type      |
|zip                |string type      |
|deliveryPoint      |string type      |
|country            |string type      |
|telephone1         |string type      |
|telephone2         |string type      |
|fax                |string type      |
|email              |string type      |

#### <a name="DateTimeType"></a>Date time type

Dates and times are formatted to the ISO-8601 standard unless specified otherwise. Information about ISO-8601 can be found
[here.](https://en.wikipedia.org/wiki/ISO_8601#Time_zone_designators) For example if the date and time June 6 2016 at 11:56 AM were
formatted to ISO-8601 you would get `"2016-06-08T11:56:22.0097345Z"` If Jun 6 2016 at 12:00 AM were
formatted to IS0-8601 you would get `"2016-06-08T00:00:00Z"`.

Business Tax Collection Mutations
---------------------------------

There are three mutations available to update data via the GraphQL schema. These are `calculate_tax_return`,
`tax_return`, and `business`. These mutations can be used to re-calculate a tax return with or without
updating the database, and to generate a new PIN for a business and save the new PIN to the database.

#### Input Object Types ####

You can send objects to the `calculate_tax_return`, `tax-return`, and `business` mutations above. The types are defined below:

**<a name="BusinessInputObjectType"></a>Business Input Object type**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|id                 |integer type     |     |
|accountNumber      |integer type     |     |
|sendPaperReturn    |boolean type     |     |
|sendEmail          |boolean type     |     |

**<a name="TaxReturnInputObjectType"></a>Tax Return Input Object type**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|id                 |integer type     |     |
|returnDescription  |string type      |     |
|businessId         |integer type     |References the business that owns the tax return |
|taxReturnDetails   |[Tax Return Detail Input Object type](#TaxReturnDetailInputObjectType)  |An array of the tax return details.     |

**<a name="TaxReturnDetailInputObjectType"></a>Tax Return Detail Input Object type**

|Field name         |Type             |Notes|
|-------------------|-----------------|-----|
|id                 |integer type     |     |
|inputAmount        |double type      |     |
|adjustedAmount     |double type      |     |
|calculatedAmount   |double type      |     |
|createTransaction  |boolean type     |     |
|description        |string type      |     |

The above input types are used in mutations only. They allow the passing of objects to mutations. Not all the fields
of the input objects are required. You could specify just the `id` and `sendPaperReturn` fields when updating a 
business for example.    

#### Calculate Tax Return ####

This mutation requires a tax return passed as the only parameter. The tax return to be calculated must
already exist in the database. It does not update the database, it simply re-calculates an existing
details on the return. The actual saving and filing of the return is handled by the `tax_return` mutation.
Use this mutation to simply calculate the return in preparation for a business owner to pay their
tax bill. After the taxes are paid the return is marked as filed.

Here is an example of the GraphQL needed to perform the mutation:

```graphql
mutation M {
    calculate_tax_return(taxReturn: {
        id: 1,
        returnDescription: "New tax return description",
        businessId: 1,
        taxReturnDetails: [
            {
                id: 1,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 30000.40,
                adjustedAmount: 0.00,
            },
            {
                id: 2,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 300.40,
                adjustedAmount: 0.00
            },
            {
                id: 3,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 4,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 3.4,
                adjustedAmount: 0
            },
            {
                id: 5,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 6,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 7,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 8,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 9,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 10,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 11,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 450.3,
                adjustedAmount: 0
            },
            {
                id: 12,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 13,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 14,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 15,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 16,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 17,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 18,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 19,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 20,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 21,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 22,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 23,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 24,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 25,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 26,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 27,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 28,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 29,
                createTransaction: false,
                description: "New detail description",
                inputAmount: 0,
                adjustedAmount: 0
            }
        ]
    }),
    {
        id,
        returnDescription,
        returnStatus,
        taxReturnDetails {
            id,
            inputAmount,
            calculatedAmount
        }
    }
}

```

Notice that all the tax return detail records are required. An error will be returned if there are missing details records.
The tax return ID and all the tax return detail IDs are required. In this example the `description`,
`createTransaction`, `inputAmount`, and `adjustedAmount` fields of the tax return detail type are passed in but
you only need the `id` and `inputAmount` fields. Notice also that you can specify
the data you want returned to you after the mutation executes. In this example the calculatedAmount is
the important field returned to the caller since this was the whole point of this mutation.

####Tax Return####

The `tax_return` mutation takes two parameters, the tax return with related details and a boolean
flag called `fileReturn`. This mutation calculates the return like `calculate_tax_return` above but saves
the return to the database and if the `fileReturn` flag is true, files the return. When a tax return is
filed it can no longer be calculated and updated.

Here is an example mutuation:

```graphql
mutation M {
    tax_return(fileReturn: false, taxReturn: {
        id: 1,
        returnDescription: "New tax return description",
        businessId: 1,
        taxReturnDetails: [
            {
                id: 1,
                inputAmount: 30000.40,
                adjustedAmount: 0.00,
            },
            {
                id: 2,
                inputAmount: 300.40,
                adjustedAmount: 0.00
            },
            {
                id: 3,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 4,
                inputAmount: 3.4,
                adjustedAmount: 0
            },
            {
                id: 5,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 6,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 7,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 8,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 9,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 10,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 11,
                inputAmount: 450.3,
                adjustedAmount: 0
            },
            {
                id: 12,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 13,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 14,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 15,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 16,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 17,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 18,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 19,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 20,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 21,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 22,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 23,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 24,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 25,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 26,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 27,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 28,
                inputAmount: 0,
                adjustedAmount: 0
            },
            {
                id: 29,
                inputAmount: 0,
                adjustedAmount: 0
            }
        ]
    }),
    {
        id,
        returnDescription,
        returnStatus,
        taxReturnDetails {
            id,
            inputAmount,
            calculatedAmount
        }
    }
}
```

The above mutation will re-calculate the tax return and then save it in the database. Since the `fileReturn` flag
is set to false the tax return will remain un-filed and can be updated and saved again.

#### Business ####

The `business` mutation can only be used to generate a new PIN and to update the `sendPaperReturn` field. No
other fields on the [Business Input Object type](#BusinessInputObjectType) can be updated at this time. The mutation takes two
parameters, `business` and `generateNewPIN`. Here is an example mutation:

```graphql
mutation M {
  business(generateNewPIN: false, business: {
    id: 1,
    sendPaperReturn: true
  }) {
    id,
    accountNumber,
    managerName {
      fullName
    }  
  }
}
```

The above mutation will not generate a new PIN but will update the `sendPaperReturn` field to true. It is
only necessary to specify the value for `id` and `sendPaperReturn` for this mutation to function correctly.
