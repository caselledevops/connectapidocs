﻿Using GraphQL with Caselle Web Services
=======================================

Introduction
------------

GraphQL is a query language created by Facebook. It provides an interface between a client and a server for fetching
data and manipulating the data on the server. More information can be found [here](http://graphql.org/docs/getting-started/)
and [here](https://github.com/graphql-dotnet/graphql-dotnet).

This documentation assumes you are have a third party ID, client ID, client secret, and OAuth token and are authorized and
ready to use Caselle Web Services. If not open the CaselleOAuth.html file for instructions on how to setup OAuth authorization
with Caselle Web services.

Simple Queries
--------------

GraphQL query syntax is very simple. You specify a path that returns an entity and just the data about the entity
that you want. In the examples below we will be querying data from the Connect Business Tax Collection database.

Here is a simple query that returns information about a business:

```
query Schema {
  business(id: 1) {
    id,
    accountNumber,
    businessName
  }
}
```

This query when executed against Caselle Web Service will return the following JSON:

```json
{
  "user_warnings": [],
  "user_errors": [],
  "status": 200,
  "server_error": "",
  "body": {
    "business": {
      "id": 1,
      "businessName": "US Novelty",
      "accountNumber": 1010
    }
  }
}
```

The first thing to notice is that a GraphQL query is not JSON although it is similar to it. This query is asking for a business
where the `id` is equal to 1. If found it only wants the id, accountNumber, and businessName fields returned.

The JSON that is return by the query is standard for Caselle Web Services. There is always the user_warnings, user_errors,
status, server_error, and body fields. In this example the status is 200 meaning OK. If no business existed with an id
of 1 the status would be 404 meaning not found. A status of 401/unauthorized and 500/unhandled error are also possible.

Here is the response returned when the business is not found:

```json
{
  "user_warnings":[],
  "user_errors":["Business not found"],
  "status":404,
  "server_error":"",
  "body":{}
}
```

Notice that the body field in the response contains a business entity with just the fields requested. Returning only the fields
requested keeps the data transmitted between the client and the server to a minimum. It also allows the server to
respond to queries where a field has been removed or not yet added. The following query asks for a non-existent field
but the server handles the request:

```graphql
query Schema {
  business(id: 1) {
    id,
    accountNumber,
    businessName,
    owner
  }
}
```

```json
{
  "user_warnings": [],
  "user_errors": [],
  "status": 200,
  "server_error": "",
  "body": {
    "business": {
      "id": 1,
      "businessName": "US Novelty",
      "accountNumber": 1010
    }
  }
}
```

The server simply ignores any fields that are not defined. Because the server behaves this way third party client code can
function correctly even when interfacing with different versions of Caselle Connect. For example if two different
Caselle Connect customers are running different versions of Connect. Customer A is using a newer version of
Connect where a field was added to the business entity providing additional functionality. Customer B has not yet
installed the update. A third party using Caselle Web Services does not need to worry about the different
versions and can continue to query and update on both customer's Connect installations.

Here are some more examples showing the power of GraphQL. The following query returns the same data as the query above:

```
query Schema {
  business(accountNumber: 1010) {
    id,
    accountNumber,
    businessName,
    owner
  }
}
```

This query is looking for a business with accountNumber equal to 1010 instead of querying by id. The same data is returned regardless.
You can not query on every business field. Caselle must implement the code that performs the query and only the `id` and `accountNumber`
fields have been programmed. There is (or will be soon) documentation on what queries can be performed for each
Connect application.

Querying the Object Graph
-------------------------

GraphQL allows queries that reference related entity data. For example in Connect a business can have multiple tax returns,
and each tax return can have multiple tax return details. Other relationships also exist and can be queried. For example,
say we want to query a business and all the related tax returns so a simple grid can be displayed. You could use
the following query:

```
query Schema {
  business(id: 1) {
    id,
    accountNumber,
    businessName,
    owner,
    taxReturns {
      id,
      returnDescription,
      returnFiledDate
    }
  }
}
```

```json
{
  "user_warnings": [

  ],
  "user_errors": [

  ],
  "status": 200,
  "server_error": "",
  "body": {
    "business": {
      "taxReturns": [
        {
          "returnFiledDate": "2014-02-25T00:00:00Z",
          "returnDescription": "Jan 2009",
          "id": 3875
        },
        {
          "returnFiledDate": "2016-02-23T00:00:00Z",
          "returnDescription": "Jan 2011",
          "id": 4917
        },
        {
          "returnFiledDate": "2016-03-22T00:00:00Z",
          "returnDescription": "Feb 2011",
          "id": 4957
        },
        {
          "returnFiledDate": null,
          "returnDescription": "Mar 2011",
          "id": 4997
        }
      ],
      "id": 1,
      "businessName": "US Novelty",
      "accountNumber": 1010
    }
  }
}
```

We can also query additional related information about the business. For example this query requests data about the
business location and manager:

```
query Schema {
  business(id: 1) {
    id,
    accountNumber,
  managerName {
    fullName
  },
  location {
    streetAddress
  }
  }
}
```

And returns this:

```json
{
  "user_warnings": [],
  "user_errors": [],
  "status": 200,
  "server_error": "",
  "body": {
    "business": {
      "managerName": {
        "fullName": "Casey Fitts"
      },
      "location": {
        "streetAddress": "2142 S Main St"
      },
      "id": 1,
      "accountNumber": 1010
    }
  }
}
```

Mutations (Updates)
-------------------

The GraphQL terminology for updating data is called a mutation. A simple mutation might look like this:

```graphql
mutation M {
  business(business: {
    id: 1,
    accountNumber: 1010,
    sendPaperReturn: true,
    sendEmail: true
  }, generateNewPIN: true)
  {
    id,
    accountNumber,
    managerName {
      fullName
    },
    sendPaperReturn,
    sendEmail
  }
}
```

In this example the mutation would update the business where the id is equal to 1 and generate and assign a new PIN. For now the fields you see above (`id`, `accountNumber`, `sendPaperReturn`, and `sendEmail`) are the only fields you can specify. The data returned would look like this:

```json
{
  "user_warnings": [],
  "user_errors": [],
  "status": 200,
  "server_error": "",
  "body": {
    "business": {
      "managerName": {
        "fullName": "Casey Fitts"
      },
      "id": 1,
      "accountNumber": 1010,
      "sendPaperReturn": true,
      "sendEmail": true
    }
  }
}
```

Just like a query you can specify the data you want returned after the update is done.


You can pass objects as parameters to mutations. Here is an example that recalculates a tax return. It passes the tax return
and the tax return details as objects. Most of the tax return details were removed for brevity:

```
mutation M {
  calculate_tax_return(taxReturn: {
    id: 4997,
    returnDescription: "New tax return description",
    businessId: 1,
    taxReturnDetails: [
      {
        id: 141037,
        createTransaction: false,
        description: "New detail description",
        inputAmount: 30000.40,
        adjustedAmount: 0.00,
        comment : "This is a comment"
      },
      {
        id: 141065,
        createTransaction: false,
        description: "New detail description",
        inputAmount: 0,
        adjustedAmount: 0
      }
      ]
    }),
    {
      id,
      returnDescription,
      returnStatus,
      taxReturnDetails {
        id,
        inputAmount,
        calculatedAmount
      }
    }
}

```

Notice that not every field for the tax return and the tax return details are sent to the server. Just
enough is sent to do the calculation. When creating new entities the same will apply, you only need to transmit enough
data to perform the operation.

Schema Introspection
--------------------

Caselle's GraphQL implementation supports introspection or querying the GraphQL schema. You can execute queries
that return information about the types, the kind of types, names of fields, and the queries that can be executed.
Here is a simple example that queries all the types defined in the system:

``` graphql
query IntrospectionQueryTypeQuery {
  __schema {
    types {
      name
    }
  }
}
```

This intropection query returns the following JSON:

``` json
{  
  "__schema":{  
    "types":[  
      {  
        "name":"String"
      },
      {  
        "name":"Boolean"
      },
      {  
        "name":"Float"
      },
      {  
        "name":"Int"
      },
      {  
        "name":"ID"
      },
      {  
        "name":"Date"
      },
      {  
        "name":"Decimal"
      },
      {  
        "name":"__Schema"
      },
      {  
        "name":"__Type"
      },
      {  
        "name":"__TypeKind"
      },
      {  
        "name":"__Field"
      },
      {  
        "name":"__InputValue"
      },
      {  
        "name":"__EnumValue"
      },
      {  
        "name":"__Directive"
      },
      {  
        "name":"__DirectiveLocation"
      },
      {  
        "name":"Query"
      },
      {  
        "name":"Business"
      },
      {  
        "name":"ContactInfoType"
      },
      {  
        "name":"Name"
      },
      {  
        "name":"ParsedStreetAddress"
      },
      {  
        "name":"BusinessActivity"
      },
      {  
        "name":"TaxReturn"
      },
      {  
        "name":"TaxReturnTypeType"
      },
      {  
        "name":"Short"
      },
      {  
        "name":"Byte"
      },
      {  
        "name":"TaxReturnDetail"
      },
      {  
        "name":"Double"
      },
      {  
        "name":"TaxAgency"
      },
      {  
        "name":"BltEntity"
      },
      {  
        "name":"Mutation"
      },
      {  
        "name":"TaxReturnInputObjectType"
      },
      {  
        "name":"TaxReturnDetailInputObjectType"
      },
      {  
        "name":"BusinessInputObjectType"
      }
    ]
  }
}
```

In the JSON above you can see all the types defined in the system including "Short", "Byte", and "Double" which are 
scalar types that are not part of standard GraphQL. This particular is just an example of what is possible and not
very helpful so let's look at a query that returns information about a specific type:

``` graphql
query IntrospectionQuery {
  __type(name: "Business") {
    name
    fields {
      name
      type {
        name
        kind
      }
    }
  }
}
```

This query returns the following:

``` JSON
{  
  "__type":{  
    "name":"Business",
    "fields":[  
      {  
        "type":{  
          "name":null,
          "kind":"NON_NULL"
        },
        "name":"id"
      },
      {  
        "type":{  
          "name":null,
          "kind":"NON_NULL"
        },
        "name":"accountNumber"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"locationCity"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"locationState"
      },
      {  
        "type":{  
          "name":"ContactInfoType",
          "kind":"OBJECT"
        },
        "name":"mailingContactInfo"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"businessTelephone1"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"businessTelephone2"
      },
      {  
        "type":{  
          "name":"Name",
          "kind":"OBJECT"
        },
        "name":"managerName"
      },
      {  
        "type":{  
          "name":"ContactInfoType",
          "kind":"OBJECT"
        },
        "name":"managerContactInfo"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"managerMailRoute"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"businessName"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"businessEmail"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"businessFax"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"status"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"salesTaxId"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"alertMessage"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"businessClassifaction"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"DBA"
      },
      {  
        "type":{  
          "name":"Date",
          "kind":"SCALAR"
        },
        "name":"originationDate"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"federalId"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"stateId"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"locationCountry"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"locationDeliveryPoint"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"locationMailRoute"
      },
      {  
        "type":{  
          "name":"ParsedStreetAddress",
          "kind":"OBJECT"
        },
        "name":"location"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"mailingRoute"
      },
      {  
        "type":{  
          "name":"Name",
          "kind":"OBJECT"
        },
        "name":"mailingAttention"
      },
      {  
        "type":{  
          "name":"Date",
          "kind":"SCALAR"
        },
        "name":"businessClosedDate"
      },
      {  
        "type":{  
          "name":"BusinessActivity",
          "kind":"OBJECT"
        },
        "name":"businessActivity"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"contactGuid"
      },
      {  
        "type":{  
          "name":"Int",
          "kind":"SCALAR"
        },
        "name":"PIN"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"guid"
      },
      {  
        "type":{  
          "name":"Boolean",
          "kind":"SCALAR"
        },
        "name":"sendPaperReturn"
      },
      {  
        "type":{  
          "name":"Boolean",
          "kind":"SCALAR"
        },
        "name":"sendEmail"
      },
      {  
        "type":{  
          "name":"String",
          "kind":"SCALAR"
        },
        "name":"taxStatus"
      },
      {  
        "type":{  
          "name":null,
          "kind":"LIST"
        },
        "name":"taxReturns"
      }
    ]
  }
}
```

That's a lot of JSON but you can see all the fields that make up the Business type and particularly fields like `Name` that
are objects themselves. This next example queries the available queries:

``` graphql
query IntrospectionQueryTypeQuery {
  __schema {
    queryType {
      fields {
        name
        args {
          name
          description
          type {
            name
            kind
            ofType {
              name
              kind
            }
          }
          defaultValue
        }
      }
    }
  }
}
```

Here is the result:

``` JSON
{  
  "__schema":{  
    "queryType":{  
      "fields":[  
        {  
          "name":"business",
          "args":[  
            {  
              "type":{  
                "ofType":null,
                "name":"Int",
                "kind":"SCALAR"
              },
              "name":"id",
              "description":"business id",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"Int",
                "kind":"SCALAR"
              },
              "name":"accountNumber",
              "description":"business account number",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"salesTaxId",
              "description":"sales tax ID assigned to the business",
              "defaultValue":null
            }
          ]
        },
        {  
          "name":"business_filtered_returns",
          "args":[  
            {  
              "type":{  
                "ofType":null,
                "name":"Int",
                "kind":"SCALAR"
              },
              "name":"accountNumber",
              "description":"business account number",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"status",
              "description":"tax return status",
              "defaultValue":null
            }
          ]
        },
        {  
          "name":"business_tax_returns_by_year",
          "args":[  
            {  
              "type":{  
                "ofType":{  
                  "name":"Int",
                  "kind":"SCALAR"
                },
                "name":null,
                "kind":"NON_NULL"
              },
              "name":"accountNumber",
              "description":"business account number",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":{  
                  "name":"Int",
                  "kind":"SCALAR"
                },
                "name":null,
                "kind":"NON_NULL"
              },
              "name":"year",
              "description":"tax year",
              "defaultValue":null
            }
          ]
        },
        {  
          "name":"business_tax_returns_by_date_range",
          "args":[  
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"accountNumber",
              "description":"business account number",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"startDate",
              "description":"start date filter",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"endDate",
              "description":"end date filter",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"status",
              "description":"tax return status",
              "defaultValue":null
            }
          ]
        },
        {  
          "name":"tax_return",
          "args":[  
            {  
              "type":{  
                "ofType":{  
                  "name":"Int",
                  "kind":"SCALAR"
                },
                "name":null,
                "kind":"NON_NULL"
              },
              "name":"id",
              "description":"tax return id",
              "defaultValue":null
            }
          ]
        },
        {  
          "name":"businesses",
          "args":[  
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"businessName",
              "description":"where business name like value",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"businessStatus",
              "description":"where business status equals value",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"DBA",
              "description":"where DBA equals value",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"mailingAddress",
              "description":"where mailing contact address like value",
              "defaultValue":null
            },
            {  
              "type":{  
                "ofType":null,
                "name":"String",
                "kind":"SCALAR"
              },
              "name":"locationAddress",
              "description":"where location contact address like value",
              "defaultValue":null
            }
          ]
        }
      ]
    }
  }
}
```

From the above JSON we can see that there is a `business` query that takes three parameters; `id`, `accountNumber`, and 
`salesTaxId`. There is a query called `business_filtered_returns` that takes two parameters; `accountNumber` and `status`. 
The description of the parameters is available as well so you know that the `status` parameter is for the tax return's status
and can be used to filter the tax returns returned by the query.

More information about GraphQL introspection can be found [here](http://graphql.org/learn/introspection/). You query the
GraphQL schema and find out everything about it. Tools like [GraphiQL](https://github.com/graphql/graphiql) can be setup
and allow you to perform any query the system supports with syntax highlighting and code completion.

Summary
-------

This was a very brief overview of how GraphQL works with Caselle Web Services. You will need additional detailed
documentation for each Connect application. The examples above were taken from the Business Licensing and Taxation
part of the web service. See the application specific documentation for information on how the query and
update data for each application.
