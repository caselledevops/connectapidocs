Using OAuth to Connect to Caselle Web Services
==============================================

Introduction
------------

Before any third party can use Caselle Web Services they need to contact Caselle and receive a third
party ID. This ID is unique to each third party and should kept private. This ID is required to communicate with
Caselle Web Services and is passed in the HTTP header with each request.

Caselle uses OAuth to authorize third party access to Caselle Web Services. A modified form of OAuth 2 Three Legged
authorization is used as described [here](http://oauthbible.com/#oauth-2-three-legged).

Three Legged Authorization Details
-----------------------------------

The information presented on the oauthbible.com web site can seem complicated but this documentation will walk you 
through the process. The basic idea is that in order to communicate with Caselle Web Services you need five things: 
a third party id, a client ID, a client secret, the subdomain that identifies a Caselle customer, and an OAuth token. 
To get the client ID and secret you need to contact the Caselle customer you plan on working with. 
We will use the fictitious city "Anycity" for our examples.

The first step is to contact Caselle and get a third party ID and the subdomain that will be used
by the Caselle customer you are working with. The third party ID must be included in the header of
each request sent to Caselle Web Services. For example `third-party-id: 91A03619-107F-25B1-09E7-1C28B793A214` would
be added to the header. The subdomain is assigned by Caselle for each of our customers who use web services.

Next you need to contact Anycity and ask them for a client ID and client secret. 
An example ID and secret would be `501e6bf0-a115-41fa-9462-46e3c043ef5f` and `Nnj5neRnO6iOO2ygShcUyYFYfVXsH` 
respectively. For Ancycity to generate the ID and secret you'll need to provide a callback URL to them. This 
URL must use HTTPS but does not need to be live at this point.

At this point you can also request an OAuth token from the customer. Anycity can generate the token and email it 
to you along with the client ID and secret. This is called the **manual** option and it is perfectly fine to use 
this option but it is less secure and cannot be automated. OAuth tokens are only good for six months so you'll 
need to contact Anycity again before the current token expires to get a new token. As soon as a new token 
is generated the old token is marked inactive and won't work so you may want to do this during scheduled down time.

A better option is to implement the callback URL. Here is an example of a callback URL:

<https://thirdparty.com/oauth/callback>

This URL should accept a POST request with JSON data similar to this example.

```json
{
  oauth_token: "qwsd9234jauiqerasd82354as29eajs==",
  client_id: "1234567890",
  expires_in: 120,
  subdomain: "anycity"
}
```

Your system should be able to match either the client ID or the subdomain with the correct customer you are
working with. Every Caselle customer that uses Caselle Web Services is assigned a subdomain. In our example 
Anycity is accessed using `rancher.caselleconnect.com` with a header variable called `subdomain` with a value of `anycity`. 
The value given for `expires_in` above is the number of days until the OAuth token expires.

At this point you have all the information needed to communicate with Caselle Web Services. You can now execute
GraphQL requests by executing a HTTP POST to a valid end point. The third party ID needs to be included in the header 
of each request. The third party ID is the same regardless of the Caselle customer you are interacting 
with. The client ID and OAuth token are specific to each customer. Your system needs to be able to handle
this information. 

An example URL and POST would look like this. The URL would be <https://rancher.casellconnect.com/tp/api/blt/graphql>
with the following JSON data posted:

```json
{
  oauth_token: "==Ab24asdfasASDf23348aASDFsqwer5tq2342asdfcvs",
  client_id: 1234567890,
  graphql_query: "query Schema {.....}"
}
```

The headers `third-party-id` and `subdomain` need to have valid values as well.

The following diagram shows the different steps and requests needed to get OAuth authorization working.

![Caselle Web Services OAuth Image](images/RancherOAuth.png)
