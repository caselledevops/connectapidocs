# README #

This repository contains the documentation for the Caselle Connect Web Services.

### What is this repository for? ###

* This repository contains the Caselle Connect Web Services documentation in Markdown format.
Browse the source to view the documentation. Updates to the documentation will be pushed to This
repository.
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Documents Available So Far ###

* [CaselleOAuth.md](https://bitbucket.org/caselledevops/connectapidocs/src/34b563d47519d8f0cda9227b14fa39a1ed474224/CaselleOAuth.md?at=master&fileviewer=file-view-default)

* [CaselleGraphQL.md](https://bitbucket.org/caselledevops/connectapidocs/src/a4d17314ced83eb350271a4e839e2882a65c93c8/CaselleGraphQL.md?at=master&fileviewer=file-view-default)

* [BusinessTaxGraphQLInterface.md](https://bitbucket.org/caselledevops/connectapidocs/src/46614e9afe05def57a8f60028184be59fb4d4b1e/BusinessTaxGraphQLInterface.md?at=master&fileviewer=file-view-default)

The WE-Markdown.css and WE-Markdown.js files can be useful if you wish to generate HTML documents
from the Markdown. By generating HTML the page links will function correctly.
